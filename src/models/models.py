# -*- coding: utf-8 -*-

from odoo import models, fields, api


class Application(models.Model):
    _name = 'ragnar.application'
    _description = 'A simple application model'
    _inherit = ['mail.thread']

    name = fields.Char(string='Name', tracking=True)
    description = fields.Text(string='Description', tracking=True)

    component_ids = fields.One2many(
        'ragnar.component', 'application_id', string="Components")


class Stereotype(models.Model):
    # Currently, recursion check is not enforced
    _name = 'ragnar.component.stereotype'
    _description = 'A stereotype defining traits for components.'
    _inherit = ['mail.thread']
    # _parent_store = True

    name = fields.Char(tracking=True)
    description = fields.Text(tracking=True)

    # parent_left = fields.Integer(index=True)
    # parent_right = fields.Integer(index=True)

    parent_id = fields.Many2one(
        'ragnar.component.stereotype',
        string='Parent Stereotype',
        ondelete='restrict',
        index=True)
    child_ids = fields.One2many(
        'ragnar.component.stereotype', 'parent_id', string='Child Stereotypes')

    # @api.constrains('parent_id')
    # def _check_hierarchy(self):
    #     if not self._check_recursion():
    #         raise models.ValidationError(
    #             'Error! You cannot create recursive stereotypes.')


class Component(models.Model):
    _name = 'ragnar.component'
    _description = 'A simple application component model'
    _inherit = ['mail.thread']

    name = fields.Char(tracking=True)
    description = fields.Text(tracking=True)
    application_id = fields.Many2one('ragnar.application',
                                     ondelete='cascade',
                                     string="Application",
                                     required=True,
                                     tracking=True)

    stereotype_ids = fields.Many2many('ragnar.component.stereotype', string='Stereotypes')
    vulnerability_ids = fields.Many2many('ragnar.vulnerability', string='Vulnerabilities')


class Risk(models.Model):
    _name = 'ragnar.risk'
    _description = 'An IT related risk.'
    _inherit = ['mail.thread']

    name = fields.Char(tracking=True)
    description = fields.Text(tracking=True)
    probability = fields.Selection(
        [('10', 'Low'),
         ('20', 'Medium'),
         ('30', 'High')],
        'Probability',
        required=True,
        tracking=True)
    impact = fields.Selection(
        [('10', 'Low'),
         ('20', 'Medium'),
         ('30', 'High')],
        'Impact',
        required=True,
        tracking=True)

    vulnerability_ids = fields.Many2many('ragnar.vulnerability', string='Vulnerabilities')


class VulnerabilityStage(models.Model):
    _name = 'ragnar.vulnerability.stage'
    _description = 'Vulnerability Stage'
    _order = 'sequence,name'

    name = fields.Char(required=True)
    sequence = fields.Integer(required=True)
    fold = fields.Boolean(default=False)


class Vulnerability(models.Model):
    _name = 'ragnar.vulnerability'
    _description = 'An identified vulnerability.'
    _order = 'severity desc'
    _inherit = ['mail.thread']

    name = fields.Char(required=True, tracking=True)
    description = fields.Text(tracking=True)

    color = fields.Integer('Color Index')

    severity = fields.Selection(
        [('10', 'Info'),
         ('20', 'Low'),
         ('30', 'Medium'),
         ('40', 'High'),
         ('50', 'Critical')],
        'Severity',
        required=True,
        tracking=True,
        default='30')

    @api.model
    def _default_stage(self):
        stage = self.env['ragnar.vulnerability.stage']
        return stage.search([], limit=1)

    @api.model
    def _group_expand_stage_id(self, stages, domain, order):
        return stages.search([], order=order)

    stage_id = fields.Many2one('ragnar.vulnerability.stage',
                               string='Stage',
                               default=_default_stage,
                               tracking=True,
                               group_expand='_group_expand_stage_id')

    task_id = fields.Many2one('project.task',
                              string="Project Task",
                              tracking=True,
                              required=False)

    component_ids = fields.Many2many('ragnar.component', string='Impacted Components')
    risk_ids = fields.Many2many('ragnar.risk', string='Associated Risks')
    campaign_ids = fields.Many2many('ragnar.auditcampaign', string='Detected in campaigns')


class AuditCampaign(models.Model):
    _name = 'ragnar.auditcampaign'
    _description = 'An identified vulnerability.'
    _inherit = ['mail.thread']

    name = fields.Char(tracking=True)
    description = fields.Text(tracking=True)
    category = fields.Selection(
        [('10', 'Code Review'),
         ('20', 'Pentest')],
        'Category',
        tracking=True,
        required=True)

    auditor = fields.Char(required=True, tracking=True)
    vulnerability_ids = fields.Many2many('ragnar.vulnerability', string='Detected Vulnerabilities')
