# -*- coding: utf-8 -*-
{
    'name': "IT Governance",

    'summary': """
        A Governance, Risk and Compliance (GRC) solution.""",

    'description': """
        This module allows the modern IT executive to have an
        overview of his application landscape, risk management
        and other useful things.
    """,

    'author': "Alcibiade Corp.",
    'website': "http://www.alcibiade.org/",
    'application': True,

    # Categories can be used to filter modules in modules listing
    # Check https://github.com/odoo/odoo/blob/12.0/odoo/addons/base/data/ir_module_category_data.xml
    # for the full list
    'category': 'Uncategorized',
    'version': '0.1',

    # any module necessary for this one to work correctly
    'depends': ['base', 'project', 'mail'],

    # always loaded
    'data': [
        'security/ir.model.access.csv',
        'views/views.xml',
        'views/templates.xml',
        'data/vulnerability_stage.xml',
    ],
    # only loaded in demonstration mode
    'demo': [
        'demo/demo.xml',
    ],
}
