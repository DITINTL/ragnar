= Ragnar IT Governance

== Overview

This application is a complete IT Governance companion targeted at
any executive manager. It allows tracking risk, compliance, budget and other subjects.

== Setup

This application is an Odoo module. It can be deployed either:

* As a standalone application with Odoo runtime and database embedded
* As part of an existing Odoo installation

=== Standalone docker setup

The Standalone image runs an embedded PostgreSQL instance that can
be loaded with default sample data.

Package the application as a docker image:

----
./docker/standalone/build.sh 
----

Start the image:

----
./docker/standalone/start.sh
----

Then open your browser to http://localhost/ and use the Odoo initial database
initialization form with your own parameters.

View logs:

----
./docker/standalone/logs.sh
----

Stop the image:

----
./docker/standalone/stop.sh
----

=== Independent database and runtime with docker-compose

Build the base images:

----
./docker/runtime/build.sh
----

Provision and start the infrastructure:

----
docker-compose -p odoo -f ./docker/compose/docker-compose.yml up -d
----

Stop the containers:

----
docker-compose -p odoo -f ./docker/compose/docker-compose.yml down
----

=== Build against Odoo sources from GIT

This document describes installation on Debian 10 Buster. It should work on debian-based distributions
and easy to adapt to other systems.

Install build pre-requisites:

----
apt-get update
apt-get install -y python3 python3-venv python3-pip python3-psycopg2 git curl libsasl2-dev python-dev libldap2-dev libssl-dev
----

Create a development folder and then prepare a dedicated python virtual environment:

----
python3 -m venv odoo-venv
source odoo-venv/bin/activate
----

Clone the development code:

----
git clone https://github.com/odoo/odoo.git -b 13.0  --depth=1
git clone https://gitlab.com/alcibiade/ragnar
----

Build and install:

----
pip install -r odoo/requirements.txt
pip install ./odoo/
----

Run a local database. It is possible to run it in the development environment directly but
it is preferable to use a Docker image.

----
docker run -d --rm --name db -p5432:5432 -e POSTGRES_USER=odoo -e POSTGRES_PASSWORD=odoo -e POSTGRES_DB=odoo postgres:10

# to run an interactive database client in the container
docker exec -it db su - postgres -c "psql -U odoo"

# to stop and erase the database later
docker stop db
----

Create a $HOME/.odoorc file mathing your environment:

----
[options]
addons_path = /home/........./odoo-dev/odoo/addons
admin_passwd = admin
db_host = localhost
db_name = odoo
db_password = odoo
db_port = 5432
db_user = odoo
----

Create a symbolic link in the odoo-dev/odoo/addons to your ragnar source folder:

----
ln -s ../../ragnar/src odoo/addons/ragnar
----

Now you can run your test odoo instance:

----
odoo -i ragnar
----

The instance is listening in HTTP on local port 8069 by default.
Feel free to customize your default admin password in the .odoorc file.

=== Existing Odoo installation

Go to the Odoo marketplace and find the Ragnar IT Management application, install it
from there.
