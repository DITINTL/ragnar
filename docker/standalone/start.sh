#!/bin/bash

set -e -x

docker run -d --rm --name ragnar-01 -p80:8069 ragnar-standalone
