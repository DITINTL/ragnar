FROM debian:buster

# Prepare the base OS
RUN apt-get update
RUN apt-get install -y python3 python3-pip python3-psycopg2 git wget curl libsasl2-dev python-dev libldap2-dev libssl-dev
RUN pip3 install num2words

# Add Dockerize from binary distribution
ENV DOCKERIZE_VERSION v0.6.1
RUN wget https://github.com/jwilder/dockerize/releases/download/$DOCKERIZE_VERSION/dockerize-linux-amd64-$DOCKERIZE_VERSION.tar.gz \
    && tar -C /usr/local/bin -xzvf dockerize-linux-amd64-$DOCKERIZE_VERSION.tar.gz \
    && rm dockerize-linux-amd64-$DOCKERIZE_VERSION.tar.gz

# Create the working directory
RUN mkdir /work
WORKDIR /work

# Install all required component
RUN git clone https://github.com/odoo/odoo.git -b 13.0  --depth=1
RUN pip3 install -r ./odoo/requirements.txt
RUN pip3 install ./odoo/
RUN mkdir -p custom_addons/ragnar
COPY ./src/ custom_addons/ragnar
COPY ./scripts/* /bin

# Environment and configuration templates
ENV DB_HOST=db
ENV DB_NAME=odoo
ENV DB_USER=odoo
ENV DB_PASSWORD=odoo
ENV DB_PORT=5432
ENV ADMIN_PASSWD=False
ENV LIST_DB=False

COPY ./templates/odoorc.tpl .

# Entrypoint

CMD dockerize -template odoorc.tpl:odoorc \
          odoo \
              --addons-path=odoo/addons,custom_addons \
              -c ./odoorc \
              -i ragnar --without-demo all
