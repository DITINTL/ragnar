#!/bin/bash

set -e -x

SCRIPT_FOLDER=$(dirname $(readlink -f "$0"))
BASE_FOLDER=$(readlink -f "${SCRIPT_FOLDER}/../..")

docker build -t ragnar-runtime -f ${SCRIPT_FOLDER}/Dockerfile ${BASE_FOLDER}
